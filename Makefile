.PHONY: all clean release

all: shellarts.exe

release: ShellArts.zip

ShellArts.zip: ShellArts
	7z a ShellArts.zip ShellArts
	rm -rf ShellArts

ShellArts: shellarts.exe
	mkdir ShellArts
	mv plug-ins ShellArts
	mv shellarts.exe ShellArts
	cp shellarts.nako ShellArts
	cp README.md ShellArts
	cp Makefile ShellArts

shellarts.exe: shellarts.nako
	nakomake shellarts.nako -t vnako -p dnako -p nakofile -p nakoctrl

clean:
	rm -rf ShellArts.zip shellarts.exe ShellArts plug-ins
